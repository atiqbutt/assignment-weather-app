package com.example.android.weather.ui.list;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import com.example.android.weather.data.WeatherRepository;
import com.example.android.weather.data.database.ListWeatherEntry;
import java.util.List;
/**
 * {@link ViewModel} for {@link MainActivity}
 */
class MainActivityViewModel extends ViewModel {
    private final WeatherRepository mRepository;
    private final LiveData<List<ListWeatherEntry>> mForecast;

    public MainActivityViewModel(WeatherRepository repository) {
        mRepository = repository;
        mForecast = mRepository.getCurrentWeatherForecasts();
    }
    public LiveData<List<ListWeatherEntry>> getForecast() {
        return mForecast;
    }
}
