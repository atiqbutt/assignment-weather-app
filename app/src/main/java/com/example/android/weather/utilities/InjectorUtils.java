/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.weather.utilities;
import android.content.Context;
import com.example.android.weather.AppExecutors;
import com.example.android.weather.data.WeatherRepository;
import com.example.android.weather.data.database.WeatherDatabase;
import com.example.android.weather.data.network.WeatherNetworkDataSource;
import com.example.android.weather.ui.detail.DetailViewModelFactory;
import java.util.Date;
import com.example.android.weather.ui.list.MainViewModelFactory;
/**
 * Provides static methods to inject the various classes needed for Sunshine
 */
public class InjectorUtils {

    public static WeatherRepository provideRepository(Context context) {
        WeatherDatabase database = WeatherDatabase.getInstance(context.getApplicationContext());
        AppExecutors executors = AppExecutors.getInstance();
        WeatherNetworkDataSource networkDataSource =
                WeatherNetworkDataSource.getInstance(context.getApplicationContext(), executors);
        return WeatherRepository.getInstance(database.weatherDao(), networkDataSource, executors);
    }
    public static WeatherNetworkDataSource provideNetworkDataSource(Context context) {
        // This call to provide repository is necessary if the app starts from a service - in this
        // case the repository will not exist unless it is specifically created.
        provideRepository(context.getApplicationContext());
        AppExecutors executors = AppExecutors.getInstance();
        return WeatherNetworkDataSource.getInstance(context.getApplicationContext(), executors);
    }

    public static DetailViewModelFactory provideDetailViewModelFactory(Context context, Date date) {
        WeatherRepository repository = provideRepository(context.getApplicationContext());
        return new DetailViewModelFactory(repository, date);
    }
    public static MainViewModelFactory provideMainActivityViewModelFactory(Context context) {
        WeatherRepository repository = provideRepository(context.getApplicationContext());
        return new MainViewModelFactory(repository);
    }


}