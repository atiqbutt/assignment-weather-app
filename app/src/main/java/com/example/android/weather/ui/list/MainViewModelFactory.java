package com.example.android.weather.ui.list;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import com.example.android.weather.data.WeatherRepository;

/**
 * Factory method that allows us to create a ViewModel with a constructor that takes a
 * {@link WeatherRepository}
 */
public class MainViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    private final WeatherRepository mRepository;
    public MainViewModelFactory(WeatherRepository repository) {
        this.mRepository = repository;
    }
    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new MainActivityViewModel(mRepository);
    }
}